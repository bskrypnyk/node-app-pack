var URL = require( 'url' );

exports.url_to_log = function( req ) {
	var ip_address = '';
	if(req.headers['x-forwarded-for']){
	    ip_address = req.headers['x-forwarded-for'];
	}
	else {
	    ip_address = req.connection.remoteAddress;
	}
	return req.url + ' from: ' + ip_address + ' method: ' + req.method;	
};

exports.log_time = function( req ) {
	console.log( label + ': ' + ( new Date().getTime() - req._startime ) + ' ms' );
};


/**
	express style middleware for insuring the url doesn't contain 
	'..', '.' or sequence of '//'s
*/
exports.normalize_url = function( req, res, next ) {

	function _normalize_url( urlpath ) {
	
		var path = unescape(urlpath);
		var segments = path.split( /\// );
		var sanitized = [];
		var redirRequired = false;
		
		// we start with '1' to ignore the leading '/'
		for( var i=1; i<segments.length; ++i ) {
			if( segments[i] == '.' ) {
				redirRequired = true;
				continue;
			}
			if( segments[i] == '..' ) {
				sanitized.pop();
				redirRequired = true;
				continue;
			}
			if( segments[i] == '' ) {
				redirRequired = true;
				continue;
			}
			
			sanitized.push( segments[i] );
		}
	
		if( redirRequired ) {
			return '/' + sanitized.join( '/' );
		}
		else {
			return null;
		}
	}
	

	var normalized = _normalize_url( req.path );
	
	if( normalized ) {
		var location = normalized;
		var url = URL.parse(req.url);
		if( url.query ) {
			location += '?' + url.query;
		}
		res.redirect( 301, location );
		return;
	}

	next();
};

