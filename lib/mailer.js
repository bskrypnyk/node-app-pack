var _ = require( 'underscore' );
var nodemailer = require( 'nodemailer' );

var settings = {};
var transport = null;

exports.init = function( smtp_settings ) {

	_.extend( settings, smtp_settings );
	transport = nodemailer.createTransport("SMTP", settings );
}

exports.send = function( to, from, subject, text, html, callback ) {
	
	if( !transport ) {
		callback( new Error( 'Please call init() on mailer with smtp settings before using it.' ) );
		return;
	}
	
	var email = {};
	if( to ) email.to = to;
	if( from ) email.from = from;
	if( subject ) email.subject = subject;
	if( text ) email.text = text;
	if( html ) email.html = html;
	
	transport.sendMail( email, function( err, result ) {
		if( callback ) {
			callback( err, result );
		}
	});	
};