var crypto = require( 'crypto' );
var _ = require( 'underscore' );

var RESET_PASSWORD_HASH_SECRET = "axl8sksdfsl209afadf";
var RESET_PASSWORD_DEFAULT_EXPIRE = 1000 * 60 * 60 * 24 * 2; // 2 days
var SALT_BYTES_LENGTH = 4; // 4 byts

/** 
	@param1 : options may contain:
	"reset_password_hash_secret" as a string
	"reset_password_default_expire_ms" as a milliseconds interval
*/
function init( options ) {
	
	options = options || {};
	
	if( options.reset_password_hash_secret ) {
		RESET_PASSWORD_HASH_SECRET = options.reset_password_hash_secret;
	}
	if( options.reset_password_default_expire_ms ) {
		RESET_PASSWORD_DEFAULT_EXPIRE = options.reset_password_default_expire_ms;
	}
	if( options.salt_bytes_length ) {
		SALT_BYTES_LENGTH = options.salt_bytes_length;
	}
}


function hash_to_parts( hash ) {

	var part1 = null;
	var part2 = null;
	var part3 = null;
	
	if( !hash ) {
		return null;
	}
	
	// we are doing indexOf, because of a corner case where user_id may contain the '|'
	
	var idx = hash.indexOf( '|' );
	if( idx == -1 ) {
		console.log( "ERROR: invalid hash: " + hash + " in authlib.hash_to_parts (1)" );
		return null;
	}

	part1 = hash.substring( 0, idx );
	var sub = hash.substring( idx+1 );
	
	idx = sub.indexOf( '|' );
	if( idx == -1 ) {
		console.log( "ERROR: invalid hash: " + hash + " in authlib.hash_to_parts (2)" );
		return null;
	}
	
	part2 = sub.substring( 0, idx );
	part3 = sub.substring( idx+1 );

	return [part1,part2,part3];
}

function userid_from_hash( hash ) {
	var parts = hash_to_parts( hash );
	return parts[2];
}

function do_hash( secret, salt ) {
	var hmac = crypto.createHmac( 'md5', salt );
	hmac.update( secret );
	return hmac.digest( 'hex' );
}

function login_hash( user_id, secret, timestamp, token ) {
	
	var tk = token || crypto.randomBytes(4).toString('hex');
	var tm = timestamp || new Date().getTime();
	var salt = tk + '.' + tm;
		
	var hash = do_hash( secret, salt );
	
	hash += '|' + tm + '|' + user_id;
	
	return {
		token: tk,
		hash: hash	
	};
}

function login_verify( user_id, secret, token, hash, max_age_ms ) {
	
	if( _.isNull(user_id) || _.isUndefined(user_id) || !secret || !token || !hash ) {
		console.log( "ERROR: invalid parameters to authlib.login_verify" );
		return false;
	}
	
	max_age_ms = max_age_ms || 0;
	
	var auth_hash = null;
	var timestamp = null;
	var hash_user_id = null;
	
	var parts = hash_to_parts( hash );
	if( parts == null ) {
		return false;
	}
	
	auth_hash = parts[0];
	timestamp = parseInt(parts[1]);
	hash_user_id = parts[2];
	
	if( !auth_hash ) {
		console.log( "ERROR: invalid hash: " + hash + " in authlib.login_verify (auth_hash)" );
		return false;
	}
	if( isNaN( timestamp ) ) {
		console.log( "ERROR: invalid hash: " + hash + " in authlib.login_verify (timestamp)" );
		return false;
	}
	if( hash_user_id == null ) {
		console.log( "ERROR: invalid hash: " + hash + " in authlib.login_verify (user_id)" );
		return false;
	}
	
	if( hash_user_id != user_id ) {
		return false;
	}
	
	var verify_hash = do_hash( secret, token + '.' + timestamp );
	if( verify_hash != auth_hash ) {
		return false;
	}
	
	if( max_age_ms > 0 ) {
		if( timestamp + max_age_ms < new Date().getTime() ) {
			return false;
		}
	}
	
	return true;
}

function reset_password_token( user_id, secret, expire_ms ) {

	expire_ms = expire_ms || RESET_PASSWORD_DEFAULT_EXPIRE;

	var expire = new Date().getTime() + expire_ms;
	var hash = do_hash( user_id + '.' + secret + '.' + RESET_PASSWORD_HASH_SECRET, expire.toString() );
	return hash + '|' + expire + '|' + user_id;
}

function reset_password_token_userid( token ) {
	
	var parts = hash_to_parts( token );
	if( parts == null ) {
		return null;
	}
		
	var hash = parts[0];
	var expire = parseInt(parts[1]);
	var user_id = parts[2];
	
	if( !hash || isNaN(expire) || !user_id ) {
		console.log( "ERROR: invalid reset password token" );
		return null;
	}
	
	if( expire < new Date().getTime() ) {
		return null;
	}
	
	return user_id;
}

function reset_password_token_verify( user_id, secret, token ) {

	var parts = hash_to_parts( token );
	if( parts == null ) {
		return false;
	}
		
	var hash = parts[0];
	var expire = parseInt(parts[1]);
	var user_id = parts[2];
	
	if( !hash || isNaN(expire) || !user_id ) {
		console.log( "ERROR: invalid reset password token" );
		return false;
	}
	
	if( expire < new Date().getTime() ) {
		return false;
	}
	
	var repeat = do_hash( user_id + '.' + secret + '.' + RESET_PASSWORD_HASH_SECRET, expire.toString() );
	
	if( hash != repeat ) {
		return false;
	}
	
	return true;
}

function remember_me_token( user_id, secret ) {
	
	var salt = crypto.randomBytes(4).toString('hex');		
	var hash = do_hash( user_id + '.' + secret, salt );
	
	return hash + '|' + salt + '|' + user_id;
}

function remember_me_token_userid( token ) {
	
	var parts = hash_to_parts( token );
	if( parts == null ) {
		return null;
	}
		
	var hash = parts[0];
	var salt = parts[1];
	var user_id = parts[2];
	
	if( !hash || !salt || !user_id ) {
		console.log( "ERROR: invalid remember me token" );
		return null;
	}
	
	return user_id;
}


function remember_me_token_verify( user_id, secret, token ) {

	var parts = hash_to_parts( token );
	if( parts == null ) {
		return false;
	}
		
	var hash = parts[0];
	var salt = parts[1];
	var user_id = parts[2];
	
	if( !hash || !salt || !user_id ) {
		console.log( "ERROR: invalid remember me token" );
		return false;
	}
	
	var repeat = do_hash( user_id + '.' + secret, salt );
	
	if( hash != repeat ) {
		return false;
	}
	
	return true;
}

exports.init = init;
exports.userid_from_hash = userid_from_hash;
exports.login_hash = login_hash;
exports.login_verify = login_verify;
exports.reset_password_token = reset_password_token;
exports.reset_password_token_userid = reset_password_token_userid;
exports.reset_password_token_verify = reset_password_token_verify;
exports.remember_me_token = remember_me_token;
exports.remember_me_token_userid = remember_me_token_userid;
exports.remember_me_token_verify = remember_me_token_verify;
