var vows = require('vows');
var assert = require('assert');
var webutils = require('../lib/index.js').webutils;

vows.describe( 'webutils' ).addBatch( {
	
	'webutils.normalize_url' : {
		topic : webutils,
		'regular path' : function( webutils ) {
		
			var path = '/foo';
			var mock_req = { url: 'http://localhost' + path, path: path };
			
			var mock_next = function() {
				// expected
				assert.ok( mock_req.url );
			}
			var mock_res = {
				redirect: function( code, location ) {
					assert.fail( location );
				}
			};
			
			webutils.normalize_url( mock_req, mock_res, mock_next );
		}	
		,
		'normalize double slashes' : function( webutils ) {
		
			var path = '//foo';
			var mock_req = { url: 'http://localhost' + path, path: path };
			
			var mock_next = function() {
				assert.fail( mock_req.url );
			}
			var mock_res = {
				redirect: function( code, location ) {
					assert.equal( location, '/foo' );
				}
			};
			webutils.normalize_url( mock_req, mock_res, mock_next );
		}
		,
		'normalize quatruple slashes' : function( webutils ) {
		
			var path = '////foo';
			var mock_req = { url: 'http://localhost' + path, path: path };
			
			var mock_next = function() {
				assert.fail( mock_req.url );
			}
			var mock_res = {
				redirect: function( code, location ) {
					assert.equal( location, '/foo' );
				}
			};
			webutils.normalize_url( mock_req, mock_res, mock_next );
		}
		,
		'normalize double slashes + query' : function( webutils ) {
		
			var path = '//foo';
			var mock_req = { url: 'http://localhost' + path, path: path };
			
			var mock_next = function() {
				assert.fail( mock_req.url );
			}
			var mock_res = {
				redirect: function( code, location ) {
					assert.equal( location, '/foo' );
				}
			};
			webutils.normalize_url( mock_req, mock_res, mock_next );
		}
		,
		'normalize double periods' : function( webutils ) {
		
			var path = '/foo/../bar';
			var mock_req = { url: 'http://localhost' + path, path: path };
			
			var mock_next = function() {
				assert.fail( mock_req.url );
			}
			var mock_res = {
				redirect: function( code, location ) {
					assert.equal( location, '/bar' );
				}
			};
			webutils.normalize_url( mock_req, mock_res, mock_next );
		}
		,
		'normalize multiple double periods' : function( webutils ) {
		
			var path = '/foo/../../../../bar';
			var mock_req = { url: 'http://localhost' + path, path: path };
			
			var mock_next = function() {
				assert.fail( mock_req.url );
			}
			var mock_res = {
				redirect: function( code, location ) {
					assert.equal( location, '/bar' );
				}
			};
			webutils.normalize_url( mock_req, mock_res, mock_next );
		}
		,
		'normalize single period' : function( webutils ) {
			var path = '/foo/./bar';
			var mock_req = { url: 'http://localhost' + path, path: path };
			
			var mock_next = function() {
				assert.fail( mock_req.url );
			}
			var mock_res = {
				redirect: function( code, location ) {
					assert.equal( location, '/foo/bar' );
				}
			};
			webutils.normalize_url( mock_req, mock_res, mock_next );

		}
		,
		'normalize multiple single periods' : function( webutils ) {
			var path = '/foo/././././bar';
			var mock_req = { url: 'http://localhost' + path, path: path };
			
			var mock_next = function() {
				assert.fail( mock_req.url );
			}
			var mock_res = {
				redirect: function( code, location ) {
					assert.equal( location, '/foo/bar' );
				}
			};
			webutils.normalize_url( mock_req, mock_res, mock_next );

		}
		,
		'normalize combination of multiple slashes, double periods, single periods' : function( webutils ) {
			var path = '/foo/.././././bar//path1////ignored///../path2';
			var mock_req = { url: 'http://localhost' + path, path: path };
			
			var mock_next = function() {
				assert.fail( mock_req.url );
			}
			var mock_res = {
				redirect: function( code, location ) {
					assert.equal( location, '/bar/path1/path2' );
				}
			};
			webutils.normalize_url( mock_req, mock_res, mock_next );

		}
	}
}).export( module );