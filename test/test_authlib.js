var vows = require('vows');
var assert = require('assert');
var authlib = require('../lib/index.js').authlib;

vows.describe('authlib').addBatch({
	'Testing login basic (non-expiring)' : {
		topic : authlib,
		'create and verify user login object' : function( authlib ) {
			var hash = authlib.login_hash( 'foo', 'bar' );
			var verify = authlib.login_verify( 'foo', 'bar', hash.token, hash.hash );			
			assert.equal( verify, true ); // expected value second
		}
		,
		'fail tampered login token' : function( authlib ) {
			var hash = authlib.login_hash( 'foo', 'bar' );
			var verify = authlib.login_verify( 'foo', 'bar', '.' + hash.token, hash.hash );
			assert.equal( verify, false );
		}
		,
		'fail tampered login hash' : function( authlib ) {
			var hash = authlib.login_hash( 'foo', 'bar' );
			var verify = authlib.login_verify( 'foo', 'bar', hash.token, '.' + hash.hash );
			assert.equal( verify, false );
		}
	}
	,
	'Testing login with expiry in future' : {
		topic: function() {
			var hash = authlib.login_hash( 'foo', 'bar' );
			var verify = authlib.login_verify( 'foo', 'bar', hash.token, hash.hash, 1000 * 10 ); // 10 seconds expiry
			this.callback( null, verify );
		},
		'verify non-expired login' : function( err, verify ) {
			assert.equal( verify, true );
		}
	}
	,
	'Testing login with expiry in past' : {
		topic: function() {
			var hash = authlib.login_hash( 'foo', 'bar' );
			var callback = this.callback;
			setTimeout( function() {
				var verify = authlib.login_verify( 'foo', 'bar', hash.token, hash.hash, 100 ); // 100ms expiry
				callback( null, verify );
			}, 200 ); // delay by 200ms to cause expiry to elapse
		},
		'fail expired login' : function( err, verify ) {
			assert.equal( verify, false ); // should fail to verify
		}
	}
	,
	'Testing reset password userid (non-expired)' : {
		topic: function() {
			var token = authlib.reset_password_token( 'foo', 'bar' ); // default expire
			this.callback( null, authlib.reset_password_token_userid( token ) );
		},
		'verify non-expired reset password userid' : function( err, user_id ) {
			assert.equal( user_id, 'foo' );
		}
	}
	,
	'Testing reset password userid (expired)' : {
		topic: function() {
			var token = authlib.reset_password_token( 'foo', 'bar', 100 ); // 100ms expire
			var callback = this.callback;
			setTimeout( function() {
				callback( null, authlib.reset_password_token_userid( token ) );
			}, 200 ); // 200ms later
		},
		'fail expired reset password token' : function( err, user_id ) {
			assert.equal( user_id, null );
		}
	}
	,
	'Testing reset password verify (non-expired)' : {
		topic: authlib,
		'verify non-expired reset password token' : function( authlib ) {
			var token = authlib.reset_password_token( 'foo', 'bar' ); // default expire
			var user_id = authlib.reset_password_token_userid( token );
			assert.equal( user_id, 'foo' );
			assert.equal( authlib.reset_password_token_verify( 'foo', 'bar', token ), true );
		}
	}
	,
	'Testing reset password verify (tampered)' : {
		topic: authlib,
		'verify reset password tampered' : function( authlib ) {
			var token = authlib.reset_password_token( 'foo', 'bar' );
			var userid = authlib.reset_password_token_userid( token );
			assert.equal( authlib.reset_password_token_verify( 'foo', 'bar', '+' + token ), false );

		}
	}
	,
	'Testing remember me token valid' : {
		topic: authlib,
		'verify remember me token' : function( authlib ) {
			var token = authlib.remember_me_token( 'foo', 'bar' );
			var userid = authlib.remember_me_token_userid( token );
			assert.equal( userid, 'foo' );
			assert.equal( authlib.remember_me_token_verify( 'foo', 'bar', token ), true );
		}
	}
	,
	'Testing remember me token (tampered)' : {
		topic: authlib,
		'fail tampered remember me token' : function( authlib ) {
			var token = authlib.remember_me_token( 'foo', 'bar' );
			var userid = authlib.remember_me_token_userid( '.' + token );
			assert.equal( userid, 'foo' );
			assert.equal( authlib.remember_me_token_verify( 'foo', 'bar', '.' + token ), false );
		}
	}
	
}).export(module);