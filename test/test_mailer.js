var vows = require('vows');
var assert = require('assert');
var _ = require( 'underscore' );
var mailer = require( '../lib/index.js' ).mailer;

vows.describe( 'Test mailer' ).addBatch( {
	
	'Test non-inited mailer' : {
		topic: function() {
			var callback = this.callback;
			mailer.send( 'foo@bar.com', 'bar@foo.com', 'Subject', 'text', 'html', function(err ) {
				callback( null, err );
			} );
		},
		'fail non-inited mailer' : function( ignore, err ) {
			assert.equal( true, _.isObject( err ), 'Error must be thrown without mailer init() called first' );
		}
	}
	
}).export( module );